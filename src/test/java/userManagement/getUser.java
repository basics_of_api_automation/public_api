package userManagement;

import core.StatusCode;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.hamcrest.Matcher;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.testng.AssertJUnit.assertEquals;

public class getUser {

    @Test
    public void getUserData(){
        given()
                .when()
                .get("https://reqres.in/api/users?page=2")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void validateGetResponseBody(){
        RestAssured.baseURI="https://jsonplaceholder.typicode.com";

        given()
                .when()
                .get("/todos/1")
                .then()
                .assertThat()
                .statusCode(200)
                .body(not(isEmptyString()))
                .body("title",equalTo("delectus aut autem"))
                .body("userId",equalTo(1));
    }

    @Test
    public void validateResponseHasItems(){
        RestAssured.baseURI="https://jsonplaceholder.typicode.com";

        Response response =given()
                .when()
                .get("/posts")
                .then()
                .extract()
                .response();

        assertThat(response.jsonPath().getList("title"), hasItem("sunt aut facere repellat provident occaecati excepturi optio reprehenderit"));

    }

    @Test
    public void validateResponseHasSize(){
        RestAssured.baseURI="https://jsonplaceholder.typicode.com";

        Response response =given()
                .when()
                .get("/comments")
                .then()
                .extract()
                .response();

        assertThat(response.jsonPath().getList(""),hasSize(500));
    }
    @Test
    public void validateResponseContains() {
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";

        Response response = given()
                .when()
                .get("/comments?postId=1")
                .then()
                .extract()
                .response();
        List<String> expectedEmails = Arrays.asList("Eliseo@gardner.biz","Jayne_Kuhic@sydney.com","Nikita@garfield.biz", "Lew@alysha.tv", "Hayden@althea.biz");

      //  assertThat(response.jsonPath().getList("email"), contains(expectedEmails.toArray((new String[0]))));
    }
    @Test
    public void validateDataValueResponse(){
        RestAssured.baseURI="https://reqres.in";

        Response response =given()
                .when()
                .get("/api/users?page=2")
                .then()
                .extract()
                .response();

        response.then().body("data[0].id",is(7));
        response.then().body("data[0].email",is("michael.lawson@reqres.in"));
        response.then().body("data[0].first_name",is("Michael"));
        response.then().body("data[0].last_name",is("Lawson"));
        response.then().body("data[0].avatar",is("https://reqres.in/img/faces/7-image.jpg"));
    }

    public void validateQueryParam() {

        Response response = given()
                .queryParam("page",2)
                .when()
                .get("https://reqres.in/api/users")
                .then()
                .extract()
                .response();

        int responseStatusCode = response.statusCode();
        assertEquals(responseStatusCode,200);

    }

    @Test
    public void validateMultipleQueryParam(){

        Response resp= given()
                .queryParam("page",2)
                .queryParam("per_page",3)
                .when()
                .get("https://reqres.in/api/users");
    }

    @Test
    public void validatePathParam(){

        String raceSeasonValue = "2017";

        Response response = given()
                .pathParam("raceSeason",raceSeasonValue)
                .when()
                .get("https://ergast.com/api/f1/{raceSeason}/circuits.json")
                .then()
                .extract()
                .response();

        int responseStatusCode = response.statusCode();
        assertEquals(responseStatusCode,200);

        System.out.println(response.body().asString());

    }

    @Test
    public void validateFormParam(){


        Response response = given()
                .contentType("application/x-www-form-urlencoded;charset=utf-8")
                .formParam("job","Dev", "name","John Doe")
                .when()
                .post("https://reqres.in/api/users")
                .then()
                .statusCode(201)
                .extract()
                .response();

        System.out.println(response.body().asString());

    }

    @Test
    public void getListHeader(){

         given().
                header("Content-Type","application/json")
                .header("Authorization","Bearer fhjdgjdgjgjjgd")
                .when()
                .get("https://reqres.in/api/users?page=2")
                .then()
                .statusCode(200);
    }

    @Test
    public void getListHeaderWithTwoMap(){

        Map<String,String> headers = new HashMap<>();
        headers.put("Content-Type","application/json");
        headers.put("Authorization","Bearer fhjdgjdgjgjjgd");

        given().
                headers(headers)
                .when()
                .get("https://reqres.in/api/users?page=2")
                .then()
                .statusCode(200);
    }

    @Test
    public void testResponseHeaders(){

        Response resp=given()
                .when()
                .get("https://reqres.in/api/users?page=2")
                .then()
                .extract().response();

        Headers headers = resp.getHeaders();
        for(Header h:headers)
        {
            System.out.println(h.getName()+":"+ h.getValue());
        }

    }

    @Test
    public void validateResponseWithBasicAuth(){
        Response resp=given()
                .auth()
                .basic("postman","password")
                .when()
                .get("https://postman-echo.com/basic-auth");

        int actualStatusCode=resp.statusCode();
        assertEquals(actualStatusCode,200);
        System.out.println(resp.body().asString());
    }

    @Test
    public void validateResponseWithDigestAuth(){
        Response resp=given()
                .auth()
                .digest("postman","password")
                .when()
                .get("https://postman-echo.com/digest-auth");

        int actualStatusCode=resp.statusCode();
        assertEquals(actualStatusCode, StatusCode.SUCCESS.code);
        System.out.println(resp.body().asString());
    }

    @Test
    public void validateResponseWithDeleteMethod(){
        Response resp=given()
                .when()
                .delete("https://reqres.in/api/users");

        int actualStatusCode=resp.statusCode();
        assertEquals(actualStatusCode,StatusCode.NO_CONTENT.code);
        System.out.println(resp.body().asString());
    }
}

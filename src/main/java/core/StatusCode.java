package core;

public enum StatusCode {

    SUCCESS(200,"OK"),
    CREATED(201,"CREATED"),

    BAD_REQUEST(400,"BAD_REQUEST"),

    UNAUTHORIZED(401,"UNAUTHORIZED"),

    NOT_FOUND(404,"NOT_FOUND"),

    NO_CONTENT(204,"NO_CONTENT");

    public final int code;
    public final String msg;

    StatusCode(int code, String msg){
        this.code=code;
        this.msg=msg;
    }
}
